
# gcp-k8s-btc-1-docker

A Docker image for experimenting with a bitcoind node in the form of a scalable container-ised application.

Based upon https://hub.docker.com/r/jamesob/bitcoind .
(Follow the link for full installation and usage instructions.)

Enhancements:

 * Support for prune with the `BTC_PRUNE` environment variable
 * Addition of a script `pre-stop.sh` which can be used as e.g. a Kubernetes hook to ensure graceful shutdown i.e. blockchain data to be flushed to disk
 * Addition of a script `web.sh` which launches a simple Node web server, used to inspect chain info such as block indexing progress

 These enhancements were all achieved through modifications to the `entrypoint.sh` script given their dependence upon the chosen location of the bitcoind installation.




